function checkGuess() {
    let userValue = parseInt(userGuess.value);
    if (userValue === randomNum) {
        message.innerText = "Congratulations, you win!";
    } else if (userValue > randomNum) {
        message.innerText = "Too high, try again!";
    } else if (userValue < randomNum) {
        message.innerText = "Too low, try again!";
    }
    attempts++;
    attemptsText.innerText = `Attempts: ${attempts}`;
}

const randomNum = Math.floor(Math.random() * 10) + 1;
const userBtn = document.getElementById("user-btn");
let userGuess = document.getElementById("tip");
let message = document.getElementById("msg");
let attemptsText = document.getElementById("attempts");
let attempts = 0;

userBtn.addEventListener("click", checkGuess);



